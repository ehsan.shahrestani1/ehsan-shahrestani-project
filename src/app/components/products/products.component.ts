import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
darkmode:boolean=false
  ngswitchby: string = "a"


  changeanimal(animal: string) {
    if (animal == "a") {
      this.ngswitchby = "a"
    }
    if (animal == "b") {
      this.ngswitchby = "b"
    }
    if (animal == "c") {
      this.ngswitchby = "c"
    }

  }
darkmodefunctipn(){
  this.darkmode=!this.darkmode
}
closeInformations(){
  this.ngswitchby=""
}
}
