import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/app.component';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {
  title : string = 'احسان شهرستانی ۱';
  user : User = {
    name: "ali",
    family: "ghorbani",
    image: ""
  }
  constructor() { }

  ngOnInit(): void {
  }

}
