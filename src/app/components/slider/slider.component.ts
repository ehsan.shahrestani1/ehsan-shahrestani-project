import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/app.component';


@Component({
  selector: 'app-slider',
  templateUrl:'./slider.component.html',
  styleUrls: ['./slider.component.css']
})
export class SliderComponent implements OnInit {
  cars :User[] =[]
  constructor() { 
    this.cars.push({
      name: "ali 1",
      family: "ghorbani 1",
      image: "https://i.natgeofe.com/n/46b07b5e-1264-42e1-ae4b-8a021226e2d0/domestic-cat_thumb_2x3.jpg"
      
    })

    this.cars.push({
      name: "ali 1",
      family: "ghorbani 2",
      image: "https://post.medicalnewstoday.com/wp-content/uploads/sites/3/2020/02/322868_1100-800x825.jpg"
    })
    this.cars.push({
      name: "ali 1",
      family: "ghorbani 2",
      image: "https://images.wsj.net/im-196181?width=1280&size=1"
    })
    this.cars.push({
      name: "ali 1",
      family: "ghorbani 2",
      image: "https://pbs.twimg.com/profile_images/922203223187972096/GuIFdKHH.jpg"
    })
    
    this.cars.push({
      name: "ali 1",
      family: "ghorbani 2",
      image: "https://images.unsplash.com/photo-1536598900193-22c889cb2480?ixid=MnwxMjA3fDB8MHxwcm9maWxlLXBhZ2V8OHx8fGVufDB8fHx8&ixlib=rb-1.2.1&w=1000&q=80"
    })

  }

  ngOnInit(): void {
  }

}
